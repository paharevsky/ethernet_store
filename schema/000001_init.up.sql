CREATE TABLE IF NOT EXISTS "users" (
                                       "users_id" integer PRIMARY KEY,
                                       "first_name" varchar,
                                       "second_name" varchar,
                                       "login" varchar,
                                       "password" varchar,
                                       "email" varchar,
                                       "phone" varchar,
                                       "role_id" integer,
                                       "is_blocked" bool,
                                       "created_at" varchar,
                                       "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "products" (
                                          "products_id" integer PRIMARY KEY,
                                          "title" varchar,
                                          "description" varchar,
                                          "price" integer,
                                          "path_to_images" varchar,
                                          "discount_price" integer,
                                          "tags_id" integer,
                                          "count_in_stock" integer,
                                          "created_at" varchar,
                                          "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "categories" (
                                            "categories_id" integer PRIMARY KEY,
                                            "title" varchar,
                                            "description" varchar,
                                            "parent_id" integer,
                                            "created_at" varchar,
                                            "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "orders" (
                                        "orders_id" integer PRIMARY KEY,
                                        "user_id" integer,
                                        "status" varchar,
                                        "created_at" varchar,
                                        "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "tags" (
                                      "tags_id" integer PRIMARY KEY,
                                      "title" varchar,
                                      "created_at" varchar,
                                      "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "order_products" (
                                                "order_id" integer,
                                                "product_id" integer
);

CREATE TABLE IF NOT EXISTS "product_categories" (
                                                    "product_id" integer,
                                                    "category_id" integer
);

CREATE TABLE IF NOT EXISTS "roles" (
                                       "roles_id" integer PRIMARY KEY,
                                       "title" varchar,
                                       "description" varchar,
                                       "created_at" varchar,
                                       "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "permissions" (
                                             "permissions_id" integer PRIMARY KEY,
                                             "title" varchar,
                                             "description" varchar,
                                             "created_at" varchar,
                                             "updated_at" varchar
);

CREATE TABLE IF NOT EXISTS "role_permission" (
                                                 "role_id" integer,
                                                 "permission_id" integer
);

CREATE TABLE IF NOT EXISTS "product_tags" (
                                              "product_id" integer,
                                              "tag_id" integer
);

CREATE TABLE IF NOT EXISTS "category_parents" (
                                                  "parent_category_id" integer,
                                                  "child_category_id" integer
);

ALTER TABLE "orders" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("users_id");

ALTER TABLE "order_products" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("products_id");

ALTER TABLE "order_products" ADD FOREIGN KEY ("order_id") REFERENCES "orders" ("orders_id");

ALTER TABLE "product_categories" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("products_id");

ALTER TABLE "product_categories" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("categories_id");

ALTER TABLE "users" ADD FOREIGN KEY ("role_id") REFERENCES "roles" ("roles_id");

ALTER TABLE "role_permission" ADD FOREIGN KEY ("role_id") REFERENCES "roles" ("roles_id");

ALTER TABLE "role_permission" ADD FOREIGN KEY ("permission_id") REFERENCES "permissions" ("permissions_id");

ALTER TABLE "product_tags" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("products_id");

ALTER TABLE "product_tags" ADD FOREIGN KEY ("tag_id") REFERENCES "tags" ("tags_id");

ALTER TABLE "category_parents" ADD FOREIGN KEY ("parent_category_id") REFERENCES "categories" ("categories_id");

ALTER TABLE "category_parents" ADD FOREIGN KEY ("child_category_id") REFERENCES "categories" ("categories_id");
