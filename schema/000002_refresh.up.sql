CREATE TABLE IF NOT EXISTS user_refresh (
                                            "user_id" integer,
                                            "token" varchar,
                                            "exp_time" timestamp
);
ALTER TABLE user_refresh ADD FOREIGN KEY ("user_id") REFERENCES users ("users_id");