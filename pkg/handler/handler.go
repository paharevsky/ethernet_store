package handler

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/paharevsky/ethernet_store/pkg/service"
	"net/http"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "home")
	})

	e.GET("/catalog", func(c echo.Context) error {
		return c.String(http.StatusOK, "catalog")
	})

	e.GET("/cart", func(c echo.Context) error {
		return c.String(http.StatusOK, "cart")
	})

	e.GET("/category", func(c echo.Context) error {
		return c.String(http.StatusOK, "category")
	})

	return e
}
