package service

import "gitlab.com/paharevsky/ethernet_store/pkg/repository"

type Authorization interface {
}

type Admin interface {
}

type Role interface {
}

type Permission interface {
}

type Category interface {
}

type Product interface {
}

type Service struct {
	Authorization
	Admin
	Role
	Permission
	Category
	Product
}

func NewService(repos *repository.Repository) *Service {
	return &Service{}
}
