package repository

import "github.com/jmoiron/sqlx"

type Authorization interface {
}

type Admin interface {
}

type Role interface {
}

type Permission interface {
}

type Category interface {
}

type Product interface {
}

type Repository struct {
	Authorization
	Admin
	Role
	Permission
	Category
	Product
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{}
}
