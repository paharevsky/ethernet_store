package Ethernet_store

// Users сущность пользователя(покупатель/админ)
type Users struct {
	UsersId    int    `json:"users_id"`
	FirstName  string `json:"first_name"`
	SecondName string `json:"second_name"`
	Login      string `json:"login"`
	Password   string `json:"password"`
	Email      string `json:"email"`
	Phone      string `json:"phone"`
	RoleId     string `json:"role_id"`
	IsBlocked  string `json:"is_blocked"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
}

// Products сущность товаров в интернет магазине
type Products struct {
	ProductsId    int    `json:"products_id"`
	Title         string `json:"title"`
	Description   string `json:"description"`
	Price         int    `json:"price"`
	PathToImages  string `json:"path_to_images"`
	DiscountPrice int    `json:"discount_price"`
	TagsId        int    `json:"tags_id"`
	CountInStock  int    `json:"count_in_stock"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
}

// Categories сущность категорий для товаров
// имеет вложенность (родительская/дочерняя)
type Categories struct {
	CategoriesId int    `json:"categories_id"`
	Title        string `json:"title"`
	Description  string `json:"description"`
	ParentId     string `json:"parent_id"`
	CreatedAt    string `json:"created_at"`
	UpdatedAt    string `json:"updated_at"`
}

// Orders сущность заказа пользователя
// который складируется в корзине
type Orders struct {
	OrdersId  int    `json:"orders_id"`
	UserId    int    `json:"user_id"`
	Status    string `json:"status"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

// Tags сущность тегов товара
type Tags struct {
	TagsId    int    `json:"tags_id"`
	Title     string `json:"title"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

// Roles сущность ролей пользователя
type Roles struct {
	RolesId     int    `json:"roles_id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

// Permissions сущность прав пользователя
type Permissions struct {
	PermissionsId int    `json:"permissions_id"`
	Title         int    `json:"title"`
	Desciption    string `json:"desciption"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
}

type OrderProducts struct {
	OrderId   int
	ProductId int
}

type ProductCategories struct {
	ProductId  int
	CategoryId int
}

type RolePermission struct {
	RoleId       int
	PermissionId int
}

type ProductTags struct {
	ProductId int
	TagId     int
}
type CategoryParent struct {
	ParentCategoryId int
	ChildCategoryId  int
}
